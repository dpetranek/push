(ns push.game
  (:require [clojure.spec.alpha :as s]
            #?(:clj [orchestra.spec.test :as stest]
               :cljs [orchestra-cljs.spec.test :as stest])
            [expound.alpha :as expound]
            [clojure.set :as set]
            [push.data :as data]))


(s/fdef occupied-locs
  :args (s/cat :board ::data/board)
  :ret (s/coll-of ::data/loc :kind set? :count 10))
(defn occupied-locs
  "Return a set of locs that are occupied by pieces."
  [board]
  (-> board
      (dissoc :anchor)
      (vals)
      (->> (reduce set/union))))


(s/fdef player-setup-complete?
  :args (s/cat :board ::data/board :player ::data/player )
  :ret boolean?)
(defn player-setup-complete? [board player]
  (->> (dissoc board :anchor)
       (filter (fn [[key val]]
                 (or (and (= player :p1) (#{:p1-pushers :p1-pawns} key))
                     (and (= player :p2) (#{:p2-pushers :p2-pawns} key)))))
       (map second)
       (reduce set/union)
       (filter (partial s/valid? ::data/setup-loc))
       (empty?)))


(s/fdef setup-complete?
  :args (s/cat :past ::data/past)
  :ret boolean?)
(defn setup-complete? [past]
  (let [setup-actions (filter #(= (:type %) :place) past)]
    (>= (count setup-actions) 10)))


(s/fdef stage
  :args (s/cat :past ::data/past)
  :ret ::data/stage)
(defn stage [past]
  (if (setup-complete? past)
    :play
    :setup))


(s/fdef lookup-piece
  :args (s/cat :board ::data/board :loc (s/nilable ::data/loc))
  :ret (s/nilable ::data/piece))
(defn lookup-piece
  [board loc]
  (let [loc->piece
        (->> (vec (dissoc board :anchor))
             (reduce (fn [acc [piece-key locs]]
                       (concat acc
                               (map (fn [loc] [loc (piece-key data/pieces)]) locs)))
                     [])
             (into {}))]
    (get loc->piece loc)))


(s/fdef get-loser
  :args (s/cat :board ::data/board)
  :ret (s/nilable ::data/player))
(defn get-loser [board]
  (->> (occupied-locs board)
       (filter (partial s/valid? ::data/game-loc))
       (filter (fn [loc] (:off? (get data/graph loc))))
       (first)
       (lookup-piece board)
       :player))


(s/fdef get-neighbors
  :args (s/cat :loc ::data/loc)
  :ret  ::data/neighbors)
(defn- get-neighbors [loc]
  (:neighbors (get data/graph loc)))


(s/fdef get-neighbor-locs
  :args (s/cat :loc ::data/loc)
  :ret  (s/coll-of ::data/loc :kind set?))
(defn get-neighbor-locs [loc]
  (into #{} (vals (get-neighbors loc))))


(s/fdef get-direction
  :args (s/cat :shift ::data/shift)
  :ret (s/nilable ::data/direction))
(defn- get-direction
  "Takes a shift and gives the direction of the neighbor target from the origin"
  [[origin target]]
  (let [loc->dir (set/map-invert (get-neighbors origin))]
    (get loc->dir target)))


(s/fdef legal-play-locs
  :args (s/cat :board ::data/board
               :origin-loc ::data/game-loc)
  :ret (s/coll-of ::data/game-loc :kind set?))
(defn- legal-play-locs
  "Return a set of locs that are legal move targets for the given board and
  origin-loc during gameplay (not during setup)."
  [board origin-loc]
  (let [occupied-locs (occupied-locs board)]
    (loop [current-loc origin-loc
           visited     #{}
           q           #{}]
      (let [neighbor-locs      (-> (get-neighbors current-loc)
                                   vals
                                   set)
            open-neighbor-locs (->> (set/difference neighbor-locs occupied-locs)
                                    (map (fn [loc] (get data/graph loc)))
                                    (remove :off?)
                                    (map :loc)
                                    (into #{}))
            result             (if (= current-loc origin-loc)
                                 visited
                                 (conj visited current-loc))
            new-q              (-> q
                                   (set/union open-neighbor-locs)
                                   (set/difference result))
            next-loc           (first new-q)]
        (if next-loc
          (recur next-loc result (disj new-q next-loc))
          (conj result origin-loc))))))


(s/fdef legal-setup-locs
  :args (s/cat :board ::data/board
               :origin-loc ::data/loc)
  :ret (s/coll-of ::data/loc :kind set?))
(defn- legal-setup-locs
  [board origin-loc]
  (if-let [{:keys [player] :as piece} (lookup-piece board origin-loc)]
    (let [valid-spaces (->> data/graph
                            (vals)
                            (remove :off?) ;; remove invalid squares
                            (map :loc)
                            (filter (fn [[_ y]]
                                      (case player
                                        :p1 (<= y 4) ;; p1 must be on top
                                        :p2 (> y 4)  ;; p2 must be on bottom
                                        false)))
                            (into #{}))]
      (set/difference valid-spaces (occupied-locs board)))
    #{}))


(s/fdef action-count
  :args (s/cat :turn-log ::data/turn-log
               :action-pred (s/coll-of :action/type :kind set?))
  :ret nat-int?)
(defn action-count [turn-log action-pred]
  (->> turn-log
       (map :type)
       (filter action-pred)
       (count)))


(s/fdef calc-pushed-locs
  :args (s/cat :board ::data/board :origin ::data/game-loc :direction ::data/direction)
  :ret (s/nilable (s/coll-of ::data/game-loc :kind vector?)))
(defn- calc-pushed-locs
  "Takes a board, an origin-loc, and a direction and returns a list of pushed locs if valid or nil if not"
  [board origin direction]
  (let [occupied-locs (occupied-locs board)
        anchor        (:anchor board)]
    (loop [current-loc origin
           res         []]
      (let [next-loc (-> data/graph (get current-loc) :neighbors (get direction)) ]
        ;; short-circuit to nil if no next-loc
        (when (and next-loc (not= anchor next-loc))
          ;; only keep going if the next-loc is occupied
          (if (contains? occupied-locs next-loc)
            (recur next-loc (conj res next-loc))
            ;; only return results if you moved more than one piece
            (when (not-empty res) (vec (concat [origin] res)))))))))


(s/fdef create-push-shift
  :args (s/cat :direction ::data/direction :loc ::data/game-loc)
  :ret  (s/nilable ::data/shift))
(defn- create-push-shift [direction [x y]]
  (case direction
    :n [[x y] [x (dec y)]]
    :e [[x y] [(inc x) y]]
    :s [[x y] [x (inc y)]]
    :w [[x y] [(dec x) y]]
    nil))


(s/fdef shift-piece
  :args (s/cat :board ::data/board :origin-loc ::data/loc :target-loc ::data/loc)
  :ret  ::data/board)
(defn- shift-piece
  [board origin-loc target-loc]
  (let [piece (lookup-piece board origin-loc)
        piece-key (get data/piece-key piece)]
    (-> board
        (update piece-key disj origin-loc)
        (update piece-key conj target-loc))))


(s/fdef shift-pieces
  :args (s/cat :board ::data/board :shifts ::data/shifts)
  :ret  ::data/board)
(defn- shift-pieces [board shifts]
  (reduce (fn [board [origin-loc target-loc]]
            (shift-piece board origin-loc target-loc))
          board
          (reverse shifts)))


(s/fdef error
  :args (s/cat :state map? :action ::data/action :info ::data/message)
  :ret  (s/keys :req-un [::error]))
(defn- error [state action info]
  (assoc state :error (merge info {:action action :state state})))


(s/fdef validate-action
  :args (s/cat :state (s/keys :req-un [::data/board ::data/player ::data/turn-log])
               :action ::data/action)
  :ret (s/or :valid-action   (s/keys :req-un [::data/action])
             :invalid-action (s/keys :req-un [::data/error])))
(defmulti validate-action
  (fn [state action] (:type action)))


(defmethod validate-action :place
  [state action]
  (let [{:keys [board player]}  state
        {:keys [shifts anchor]} action
        [[origin target]]       shifts
        piece                   (lookup-piece board origin)
        stationary-anchor?      (or (nil? anchor)
                                    (= (:anchor board) anchor))
        own-piece?              (= player (:player piece))]
    (cond (not= 1 (count shifts))
          (error state action {:type :error :key :invalid-place1})
          (not stationary-anchor?)
          (error state action {:type :error :key :invalid-place2})
          (not own-piece?)
          (error state action {:type :error :key :invalid-place3})
          (not (contains? (legal-setup-locs board origin) target))
          (error state action {:type :error :key :invalid-place4})
          :else
          (assoc state :action action))))


(defmethod validate-action :move
  [state action]
  (let [{:keys [board player turn-log]} state
        {:keys [shifts anchor]}             action
        [[origin target]]                   shifts
        piece                               (lookup-piece board origin)
        stationary-anchor?                  (or (nil? anchor)
                                                (= (:anchor board) anchor))
        own-piece?                          (= player (:player piece))
        move-count                          (action-count turn-log #{:move})
        push-count                          (action-count turn-log #{:push})]
    (cond (not= 1 (count shifts))
          (error state action {:type :error :key :invalid-move1})
          (not stationary-anchor?)
          (error state action {:type :error :key :invalid-move2})
          (not own-piece?)
          (error state action {:type :error :key :invalid-move3})
          (not (contains? (legal-play-locs board origin) target))
          (error state action {:type :error :key :invalid-move4})
          (>= move-count 2)
          (error state action {:type :error :key :invalid-move5})
          (>= push-count 1)
          (error state action {:type :error :key :invalid-move6})
          :else
          (assoc state :action action))))


(defmethod validate-action :push [state action]
  (let [{:keys [board player turn-log]} state
        {:keys [shifts anchor]} action
        [[origin target]]       shifts
        piece                   (lookup-piece board origin)
        stationary-anchor?      (or (nil? anchor)
                                    (= (:anchor board) anchor))
        invalid-anchor?         (not= anchor target)
        own-piece?              (= (:player piece) player)
        push-capable-piece?     (= (:type piece) :pusher)
        push-count              (action-count turn-log #{:push})
        direction               (get-direction [origin target])
        push-shifts             (mapv (partial create-push-shift direction)
                                      (calc-pushed-locs board origin direction))]
    (cond (not direction)
          (error state action {:type :error :key :invalid-push1})
          stationary-anchor?
          (error state action {:type :error :key :invalid-push2})
          invalid-anchor?
          (error state action {:type :error :key :invalid-push3})
          (not own-piece?)
          (error state action {:type :error :key :invalid-push4})
          (>= push-count 1)
          (error state action {:type :error :key :invalid-push5})
          (not push-capable-piece?)
          (error state action {:type :error :key :invalid-push6})
          (not (seq push-shifts))
          (error state action {:type :error :key :invalid-push7})
          :else
          (assoc state :action (assoc action :shifts push-shifts)))))


(s/fdef apply-action
  :args (s/cat :board ::data/board :action ::data/action)
  :ret ::data/board)
(defn- apply-action
  "Only intended for pre-validated actions"
  [board {:keys [anchor shifts]}]
  (-> board
      (shift-pieces shifts)
      (cond-> anchor (assoc :anchor anchor))))


(s/fdef apply-actions
  :args (s/cat :board ::data/board :actions ::data/action-stream)
  :ret ::data/board)
(defn apply-actions
  "Only intended for pre-validated actions"
  [board actions]
  (reduce apply-action board actions))


(s/fdef continuing-action
  :args (s/cat :state (s/keys :req-un [::data/turn-log ::data/check-board])
               :action ::data/action)
  :ret  (s/keys :req-un [::data/action ::data/turn-log ::data/check-board ::data/board]))
(defn continuing-action
  "If a player moves a piece from one space A to B, and then moves it again from
  B to C, consolidate those moves into a single move from A to C.

  This function checks if the continuing action applies and updates the game state
  appropriately, letting it pass through if it doesn't."
  [state action]
  (let [{:keys [check-board turn-log]} state
        [old-origin old-target] (first (:shifts (last turn-log)))
        {:keys [type shifts]}   action
        [new-origin new-target] (first shifts)
        merged-action           (assoc action :shifts [[old-origin new-target]])
        new-log                 (if (seq turn-log) (pop turn-log) turn-log)]
    (if (and (= old-target new-origin)
             (#{:move :place} type))
      (-> state
          (assoc :action merged-action)
          (update :turn-log pop)
          (assoc :board (apply-actions check-board new-log)))

      (assoc state :action action))))


(s/fdef validate-and-apply-action
  :args (s/cat :state (s/keys :req-un [::data/board ::data/turn-log ::data/player])
               :action ::data/action)
  :ret (s/or :valid (s/keys :req-un [::data/board ::data/turn-log])
             :invalid (s/keys :req-un [::data/error])))
(defn validate-and-apply-action [state action]
  (let [{:keys [action] :as state}                   (continuing-action state action)
        result                                       (validate-action state action)
        board                                        (:board state)
        {[[origin target]] :shifts :as valid-action} (:action result)]
    (if (:error result)
      result
      (-> state
          (assoc :board (apply-action board valid-action))
          ;; only add to log if not returning to original spot
          (cond-> (not= origin target)
            (update :turn-log conj valid-action))))))


(s/fdef validate-and-apply-turn
  :args (s/cat :state (s/keys :req-un [::data/board ::data/turn-log ::data/player])
               :turn-log ::data/turn-log)
  :ret (s/keys :req-un [::data/board ::data/turn-log ::data/player]))
(defn validate-and-apply-turn [state turn-summary]
  (reduce (fn [state action]
            (let [next-state (validate-and-apply-action state action)]
              (if (:error next-state)
                (reduced next-state)
                next-state)))
          state
          turn-summary))


;; pretty printing
(defn- display-square [board loc]
  (let [space                 (get data/graph loc)
        {:keys [player type]} (lookup-piece board loc)
        occupied?             (occupied-locs board)]
    (cond (occupied? loc)
          (case [player type]
            [:p1 :pawn] "o"
            [:p1 :pusher] (if (= (:anchor board) loc) "Θ" "O")
            [:p2 :pawn] "x"
            [:p2 :pusher] (if (= (:anchor board) loc) "Ж" "X"))
          (:off? space) " "
          :else         "_")))


(defn print-board [board & opts]
  (let [rows (->> (partition 4 (data/generate-board-locs))
                  (map (partial map (partial display-square board)))
                  (map (partial map #(str " " %)))
                  (map #(conj (vec %2) (str " |" %1)) (range 10))
                  (map (partial reduce str)))]
    (println " 1 2 3 4")
    (doseq [row rows]
      (println row))
    (when opts board)))

(defn print-board-horizontal [board & opts]
  (let [rows (->> (partition 10 (sort-by first (data/generate-board-locs)))
                  (map (partial map (partial display-square board)))
                  (map (partial map #(str " " %)))
                  (map #(conj (vec %2) (str " |" %1)) (range 10))
                  (map (partial reduce str)))]
    (println " 0 1 2 3 4 5 6 7 8 9")
    (doseq [row rows]
      (println row))
    (when opts board)))

(stest/instrument)

#?(:clj (alter-var-root #'s/*explain-out* (constantly expound/printer)))
