(ns push.locale)

(def i18n
  {:en {:p1-wins "Player 1 Wins!"
        :p2-wins "Player 2 wins!"
        :invalid-place1 "Invalid placement"
        :invalid-place2 "Invalid placement"
        :invalid-place3 "You can't place your opponent's piece"
        :invalid-place4 "You can't place a piece there"
        :invalid-move1  "Invalid move"
        :invalid-move2  "Invalid move"
        :invalid-move3  "You can't move your opponent's piece"
        :invalid-move4  "You can't move a piece there"
        :invalid-move5  "You've used all your moves, you must push"
        :invalid-move6  "You can't do anything after you've pushed"
        :invalid-push1  "Invalid push"
        :invalid-push2  "Invalid push"
        :invalid-push3  "Invalid push"
        :invalid-push4  "You can't push with your opponent's piece"
        :invalid-push5  "You can only push once"
        :invalid-push6  "You can only push with a square piece"
        :invalid-push7  "You must push at least one other piece"
        :invalid-commit1 "You must place all your pieces on the board to complete your turn"
        :invalid-commit2 "You must push at least once to complete your turn"
        :invalid-commit3 "There was an error syncing your game"
        :invalid-commit4 "There was an error updating your gameve"
        :network-request-error "There was an error loading this game"
        :waiting-for-opponent  "Waiting for your opponent's turn..."
        }})

(defn localize [locale msg]
  (get-in i18n [locale msg]))
