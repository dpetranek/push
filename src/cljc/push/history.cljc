(ns push.history
  (:require [clojure.spec.alpha :as s]
            [push.data :as data]))

(s/fdef back
  :args (s/cat :history ::data/history)
  :ret  ::data/history)
(defn back
  "Go back one action"
  [{:keys [past] :as history}]
  (if-let [action (last past)]
    (if (not= (:type action) :place)
      (-> history (update :past pop) (update :future conj action))
      history)
    history))


(s/fdef forward
  :args (s/cat :history ::data/history)
  :ret  ::data/history)
(defn forward
  "Go forward one action"
  [{:keys [future] :as history}]
  (if-let [s (last future)]
    (-> history (update :future pop) (update :past conj s))
    history))


;; we still want to go back if we're currently on a push-action
;; so I need another arity (:recurse) to handle that case
(s/fdef back-turn
  :args (s/cat :history ::data/history :recurse-flag (s/? any?))
  :ret  ::data/history)
(defn back-turn
  ([{:keys [past] :as history}]
   (if-let [{:keys [type]} (last past)]
     (back-turn (back history) :recurse)
     history))

  ([{:keys [past] :as history} _]
   (if-let [{:keys [type]} (last past)]
     (if (#{:push :place} type)
       history
       (back-turn (back history) :recurse))
     history)))


(s/fdef forward-turn
  :args (s/cat :history ::data/history)
  :ret  ::data/history)
(defn forward-turn
  ([{:keys [future] :as history}]
   (if-let [{:keys [type]} (last future)]
     (if (= type :push)
       (forward history)
       (forward-turn (forward history)))
     history)))


(s/fdef beginning
  :args (s/cat :history ::data/history)
  :ret  ::data/history)
(defn beginning [{:keys [past] :as history}]
  (let [play-actions  (remove #(= (:type %) :place) past)
        setup-actions (filter #(= (:type %) :place) past)]
    (-> history
        (update :future #(apply conj % (reverse play-actions)))
        (assoc :past (vec setup-actions)))))


(s/fdef present
  :args (s/cat :history ::data/history)
  :ret  ::data/history)
(defn present [{:keys [future] :as history}]
  (-> history
      (update :past #(apply conj % (reverse future)))
      (assoc :future [])))
