(ns push.data
  (:require [clojure.spec.alpha :as s]))


;; locations for points in a graph
(s/def ::x (s/int-in 1 5))
(s/def ::y (s/int-in 0 10))
(s/def ::game-loc (s/tuple ::x ::y))
(s/def ::setup-loc (s/tuple zero? ::y))
(s/def ::loc (s/or :game-loc ::game-loc
                   :setup-loc ::setup-loc))
(s/def ::origin ::loc)

;; directions for graph traversal
(s/def ::n ::game-loc)
(s/def ::e ::game-loc)
(s/def ::s ::game-loc)
(s/def ::w ::game-loc)
(s/def ::neighbors (s/keys :opt-un [::n ::e ::s ::w]))
(s/def ::direction #{:n :e :s :w})


;; a game piece
(s/def ::player #{:p1 :p2})
(s/def :piece/type #{:pusher :pawn})
(s/def ::piece (s/keys :req-un [::player :piece/type]))

;; whether a space is on the board
(s/def ::off? boolean?)

(s/def ::space (s/keys :req-un [::loc ::off? ::neighbors]))

(s/def ::pushers (s/coll-of ::loc :count 3 :kind set?))
(s/def ::pawns (s/coll-of ::loc :count 2 :kind set?))
(s/def ::p1-pushers ::pushers)
(s/def ::p2-pushers ::pushers)
(s/def ::p1-pawns ::pawns)
(s/def ::p2-pawns ::pawns)
(s/def ::anchor (s/or :game-loc ::game-loc
                      :initial-loc #{:none}))
;; (s/def ::board (s/keys :req-un [::anchor]))
(s/def ::board
  (s/and
   (s/keys :req-un [::p1-pushers ::p2-pushers ::p1-pawns ::p2-pawns ::anchor])
   (fn ten-pieces? [board]
     (= 10
        (->> (vals (dissoc board :anchor))
             (apply concat)
             (into #{})
             (count))))
   (fn valid-anchor? [{:keys [anchor] :as board}]
     (as-> (vals (dissoc board :anchor)) $
       (apply concat $)
       (into #{} $)
       (cond-> $
         (s/valid? ::game-loc anchor)
         (contains? anchor))))))

(s/def ::check-board ::board)


(s/def ::shift (s/tuple ::loc ::loc))
(s/def ::shifts (s/coll-of ::shift))
(s/def :action/type #{:place :move :push})
(s/def ::action
  (s/keys :req-un [::shifts :action/type]
          :opt-un [::anchor]))

(s/def :message/type #{:error :info})
(s/def ::key  keyword?)
(s/def ::body string?)
(s/def ::message (s/keys :req-un [:message/type ::key] :opt-un [::body]))
(s/def ::error ::message)

(s/def ::action-stream (s/coll-of ::action :kind vector?))
(s/def ::past     ::action-stream)
(s/def ::future   ::action-stream)
(s/def ::turn-log ::action-stream)
(s/def ::history (s/keys :req-un [::past ::future]))

(s/def ::mode #{:active :hold})
(s/def ::stage #{:setup :play})
(s/def ::victor ::player)
(s/def ::actor ::player)
(s/def ::id nat-int?)



(defn generate-board-locs []
  (for [y (range 10) x (range 1 5)]
    [x y]))

(defn generate-setup-locs []
  (for [y (range 10)]
    [0 y]))

(defn generate-p1-setup-locs []
  (for [y (range 5)]
    [0 y]))

(defn generate-p2-setup-locs []
  (for [y (range 5 10)]
    [0 y]))

(defn generate-spaces []
  (let [offs #{[0 0] [0 1] [0 2] [0 3] [0 4] [0 5] [0 6] [0 7] [0 8] [0 9]
               [1 0] [2 0] [3 0] [4 0]
               [1 1] [4 1]
               [4 2]
               [1 7]
               [1 8] [4 8]
               [1 9] [2 9] [3 9] [4 9]}
        locs (concat (generate-board-locs)
                     (generate-p1-setup-locs)
                     (generate-p2-setup-locs))]
    (->> locs
         (map (fn [loc] {:loc loc}))
         (map (fn [space] (if (contains? offs (:loc space))
                            (assoc space :off? true)
                            (assoc space :off? false)))))))

(defn n [[x y]]
  [x (dec y)])
(defn e [[x y]]
  [(inc x) y])
(defn s [[x y]]
  [x (inc y)])
(defn w [[x y]]
  [(dec x) y])

(defn neighbor-map [loc]
  (->> ((juxt n e s w) loc)
       (zipmap [:n :e :s :w])
       (filter (comp (partial s/valid? ::game-loc) second))
       (into {})))

(defn create-graph []
  (reduce (fn [graph {:keys [loc off?] :as space}]
            (let [neighbor-map (if (s/valid? ::game-loc loc)
                                 (neighbor-map loc)
                                 {})]
              (-> graph
                  (assoc loc space)
                  (assoc-in [loc :neighbors] neighbor-map))))
          (sorted-map)
          (generate-spaces)))

(def graph (create-graph))

(def pieces
  {:p1-pushers {:player :p1 :type :pusher}
   :p2-pushers {:player :p2 :type :pusher}
   :p1-pawns   {:player :p1 :type :pawn}
   :p2-pawns   {:player :p2 :type :pawn}})

(def piece-key
  {{:player :p1 :type :pusher} :p1-pushers
   {:player :p2 :type :pusher} :p2-pushers
   {:player :p1 :type :pawn} :p1-pawns
   {:player :p2 :type :pawn} :p2-pawns})

(def setup
  {:anchor                     :none
   :p1-pushers #{[0 2] [0 3] [0 4]}
   :p2-pushers #{[0 5] [0 6] [0 7]}
   :p1-pawns   #{[0 0] [0 1]}
   :p2-pawns   #{[0 8] [0 9]}})

(def play1
  {:anchor     :none
   :p1-pushers #{[2 4] [3 4] [4 4]}
   :p2-pushers #{[1 5] [2 5] [3 5]}
   :p1-pawns   #{[1 4] [1 3]}
   :p2-pawns   #{[4 5] [4 6]}})

(def play2
  {:anchor     [4 5]
   :p1-pushers #{[2 4] [3 4] [4 5]}
   :p2-pushers #{[1 5] [2 5] [3 5]}
   :p1-pawns   #{[1 4] [1 3]}
   :p2-pawns   #{[4 6] [4 7]}})

(def victory
  {:anchor     [4 6]
   :p1-pushers #{[3 4] [2 4] [4 6]}
   :p2-pushers #{[2 5] [3 5] [1 4]}
   :p1-pawns   #{[1 2] [1 3]}
   :p2-pawns   #{[4 8] [4 7]}})

(def setup-history
  {:past
   [{:type :place, :shifts [[[0 0] [1 3]]], :id 1}
    {:type :place, :shifts [[[0 1] [1 4]]], :id 1}
    {:type :place, :shifts [[[0 2] [2 4]]], :id 1}
    {:type :place, :shifts [[[0 3] [3 4]]], :id 1}],
   :future []})
