(ns push.client
  (:require [clojure.spec.alpha :as s]
            [reagent.core :as reagent]
            [reagent.session :as session]
            [secretary.core :as secretary :include-macros true]
            [accountant.core :as accountant]
            [re-frame.core :as rf]
            [push.data :as data]
            [push.websockets :as ws]
            [push.common.components :as common]
            [push.subs :as common-sub]
            [push.events :as common-evt]
            [push.game.subs]
            [push.game.events :as game-evt]
            [push.game.views :refer [game-page]]
            [push.start.views :refer [start-page]]
            [push.tips.views :refer [tips-page]]))

(defn current-page []
  (let [modal (rf/subscribe [::common-sub/modal])]
    (fn []
      [:div [(session/get :current-page)]
       (when (:show @modal) [common/modal (:content @modal)])])))


;; Routes
(secretary/defroute "/" []
  (session/put! :current-page #'start-page))
(secretary/defroute "/tips" []
  (session/put! :current-page #'tips-page))

(secretary/defroute "/game/:id" [id query-params]
  (when-let [actor (:actor query-params)]
    (when (s/valid? ::data/actor (keyword actor))
      (rf/dispatch [::game-evt/set-actor actor])))
  (if-let [game-id (.valueOf (js/Number id))]
    (rf/dispatch [::game-evt/fetch-game game-id]))
  (session/put! :current-page #'game-page))


;; Initialize app
(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (secretary/dispatch! path))
    :path-exists?
    (fn [path]
      (secretary/locate-route path))})
  (accountant/dispatch-current!)
  (ws/make-websocket! (str (if ^boolean goog.DEBUG "ws://" "wss://") (.-host js/location) "/ws"))
  (rf/dispatch-sync [::common-evt/initialize])
  (mount-root))
