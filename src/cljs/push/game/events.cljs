(ns push.game.events
  (:require [clojure.spec.alpha :as s]
            [re-frame.core :as rf]
            [push.effects :as fx]
            [push.events :as common-evt]
            [push.data :as data]
            [push.game :as game]
            [push.history :as history]))



(rf/reg-event-fx
 ::select
 [common-evt/interceptors]
 (fn select [{:keys [db]} [_ loc piece]]
   (let [{:keys [board history origin]} db
         stage  (game/stage (:past history))]
     (case stage
       :setup {:db db :dispatch [::setup-handler loc piece]}
       :play {:db db :dispatch [::play-handler loc piece]}
       {:db db}))))


  ;; |----------+--------------+--------------------|
  ;; |          | no origin    | origin             |
  ;; |----------+--------------+--------------------|
  ;; | no piece | reset        | legal? shift-piece |
  ;; |----------+--------------+--------------------|
  ;; | piece    | select-piece | same? deselect     |
  ;; |          |              | select new piece   |
  ;; |----------+--------------+--------------------|
(rf/reg-event-fx
 ::setup-handler
 [common-evt/interceptors]
 (fn setup-handler [{:keys [db]} [_ loc piece]]
   (let [{:keys [player board origin]} db
         own-piece? (= player (:player piece))]
     (case [(boolean origin) (boolean piece)]
       [true true] (cond (= loc origin)
                         {:db (dissoc db :origin)}
                         own-piece?
                         {:db (assoc db :origin loc)}
                         :else
                         {:db db})
       [true false] {:dispatch [::place loc] :db db}
       [false true] (cond own-piece? {:db (assoc db :origin loc)}
                          :else {:db db})
       {:db (-> db (dissoc :origin) (dissoc :message))}))))


  ;; |          | no origin                 | origin                     |
  ;; |----------+---------------------------+----------------------------|
  ;; | no piece | reset                     | legal? move                |
  ;; |----------+---------------------------+----------------------------|
  ;; | piece    | own-piece? - select piece | same? deselect             |
  ;; |          |                           | neighbor? - push           |
  ;; |          |                           | own-piece select new piece |
  ;; |----------+---------------------------+----------------------------|
(rf/reg-event-fx
 ::play-handler
 [common-evt/interceptors]
 (fn play-handler [{:keys [db]} [_ loc piece]]
   (let [{:keys [player board origin]} db
         own-piece? (= player (:player piece))
         neighbor? (when origin ((game/get-neighbor-locs origin) loc))]
     (case [(boolean origin) (boolean piece)]
       [true true] (cond (= loc origin)
                         {:db (dissoc db :origin)}

                         neighbor?
                         {:dispatch [::push loc] :db db} ;; push

                         own-piece?
                         {:db (assoc db :origin loc)}

                         :else ;; noop
                         {:db db})
       [true false] {:dispatch [::move loc] :db db} ;; move
       [false true] (cond own-piece? {:db (assoc db :origin loc)}
                          :else {:db db})
       {:db (-> db (dissoc :origin) (dissoc :message))}))))

(rf/reg-event-fx
 ::place
 [common-evt/interceptors]
 (fn place [{:keys [db]} [_ loc]]
   {:db db
    :dispatch [::validate-and-apply {:type :place :shifts [[(:origin db) loc]]}]}))

(rf/reg-event-fx
 ::move
 [common-evt/interceptors]
 (fn move [{:keys [db]} [_ loc]]
   {:db db
    :dispatch [::validate-and-apply {:type :move :shifts [[(:origin db) loc]]}]}))

(rf/reg-event-fx
 ::push
 [common-evt/interceptors]
 (fn push [{:keys [db]} [_ loc]]
   {:db db
    :dispatch [::validate-and-apply {:type :push :shifts [[(:origin db) loc]] :anchor loc}]}))

(defn clean [state]
  (dissoc state :origin :message :error :action))

(rf/reg-event-fx
 ::validate-and-apply
 [common-evt/interceptors]
 (fn validate-and-apply [{:keys [db]} [_ action]]
   (let [state (game/validate-and-apply-action db action)]
     (if (:error state)
       {:dispatch [::error (:error state)] :db db}
       {:db (clean state)}))))

(rf/reg-event-fx
 ::submit
 [common-evt/interceptors]
 (fn submit [{:keys [db]} _]
   (let [{:keys [player turn-log check-board id mode board]} db]
     (cond (and (= mode :setup) (not (game/player-setup-complete? board player)))
           {:db db :dispatch [::error {:type :error :key :invalid-commit1}]}
           (and (= mode :play) (not= 1 (game/action-count turn-log #{:push})))
           {:db db :dispatch [::error {:type :error :key :invalid-commit2}]}
           :else
           {:db (-> db
                    (assoc :player (if (= player :p1) :p2 :p1))
                    (assoc :turn-log [])
                    (update-in [:history :past] (fn [h] (apply conj h turn-log)))
                    (clean))
            ::fx/ws-send {:q :update-game
                          :id id
                          :turn-log turn-log
                          :check-board check-board
                          :player player}
            :dispatch [::mode-check]}))))

(rf/reg-event-fx
 :update-game
 [common-evt/interceptors]
 (fn update-game [{:keys [db]} [_ data]]
   ;; TODO: don't broadcast to all clients
   (if (= (:id data) (:id db))
     {:db db :dispatch [::sync-state data]}
     {:db db})))

(rf/reg-event-db
 ::error
 [common-evt/interceptors]
 (fn error [db [_ message]]
   (-> db
       (assoc :message message)
       (dissoc :origin))))

(rf/reg-event-fx
 ::undo
 (fn undo [{:keys [db]} _]
   (let [{:keys [check-board turn-log]} db
         initial-state (-> db (assoc :board check-board) (assoc :turn-log []))
         undone-state (game/validate-and-apply-turn initial-state (pop turn-log))]
     (if (:error undone-state)
       {:dispatch [::error (:error undone-state)] :db db}
       {:db (clean undone-state)}))))

(rf/reg-event-fx
 ::history-transit
 [common-evt/interceptors]
 (fn history-transit [{:keys [db]} [_ traverse-key]]
   (let [history (:history db)

         traverse-command
         (case traverse-key
           :beginning    history/beginning
           :back-turn    history/back-turn
           :back         history/back
           :forward      history/forward
           :forward-turn history/forward-turn
           :present      history/present)

         {:keys [past future] :as new-history}
         (traverse-command history)

         historical-board (game/apply-actions data/setup past)]
     {:db
      (-> db
          (assoc :board historical-board)
          (assoc :history new-history)
          (assoc :mode (if (empty? future)
                         :play
                         :history)))
      :dispatch [::mode-check]})))


(rf/reg-event-db
 ::set-actor
 [common-evt/interceptors]
 (fn set-actor [db [_ actor]]
   (assoc db :actor (keyword actor))))

(rf/reg-event-fx
 ::toggle-singleplayer
 [common-evt/interceptors]
 (fn toggle-singleplayer [{:keys [db]} _]
   (let [{:keys [single-player? player actor id games]} db]
     (if (not single-player?)
       {:db (do
              (.replace js/location id)
              (-> db
                  (dissoc :messsage)
                  (assoc :mode :active)
                  (assoc :single-player? true)
                  (update-in [:storage id :single-player?] not)
                  (cond-> (not= player actor)
                    (assoc :actor player))))
        ::fx/set-local-storage ["storage" (update (:storage db) id assoc :single-player? true)]}))))


(rf/reg-event-db
 ::mode-check
 [common-evt/interceptors]
 (fn mode-check [db _]
   (let [{:keys [mode actor player board victor history single-player?]} db
         loser      (when (seq board) (game/get-loser board))
         victor     (or victor (case loser :p1 :p2 :p2 :p1 nil))
         victor-msg (get {:p1 :p1-wins :p2 :p2-wins} victor)
         new-mode   (if (empty? (:future history)) :active :hold)]
     (cond victor
           (-> db
               (assoc :victor victor)
               (assoc :message {:type :info :key victor-msg})
               (assoc :mode :hold))
           (and actor (not= actor player) single-player?)
           (-> db
               (assoc :actor (if (= actor :p1) :p2 :p1)))

           (and actor (not= actor player) (not single-player?))
           (-> db
               (assoc :mode :hold)
               (assoc :message {:type :info :key :waiting-for-opponent}))
           :else
           (assoc db :mode new-mode)))))


(rf/reg-event-fx
 ::sync-state
 [common-evt/interceptors]
 (fn sync-state [{:keys [db]} [_ new-state]]
   (let [{:keys [id check-board history victor]} new-state
         single-player?  (get-in db [:storage id :single-player?])
         turn-count      (game/action-count history #{:push})
         setup-complete? (game/setup-complete? history)
         player          (cond (and setup-complete? (even? turn-count)) :p1
                               (and setup-complete? (odd? turn-count)) :p2
                               (game/player-setup-complete? check-board :p1) :p2
                               :else :p1)]
     {:db       (-> db
                    (assoc :check-board check-board)
                    (assoc :board check-board)
                    (assoc-in [:history :past] history)
                    (assoc :player player)
                    (cond-> single-player? (assoc :actor player
                                                  :single-player? true))
                    (cond-> victor (assoc :victor victor))
                    (clean))
      :dispatch [::mode-check]})))


;; wire-talk
(rf/reg-event-fx
 ::fetch-game
 [common-evt/interceptors]
 (fn initiate-fetch-game [{:keys [db]} [_ id]]
   (let [int-id (.valueOf (js/Number id))]
     {:db (assoc db :id int-id)
      ::fx/xhttp {:method :get :url (str "/api/game/" id) :handler-key :fetch-game}})))


(rf/reg-event-fx
 :fetch-game
 [common-evt/interceptors]
 (fn handle-fetch-game [{:keys [db]} [_ data]]
   (if (= (:id data) (:id db))
     {:db db
      :dispatch [::sync-state data]}
     {:db db ::fx/location! "/"})))


(rf/reg-event-fx
 ::rematch
 [common-evt/interceptors]
 (fn initiate-rematch [{:keys [db]} [_ turn-no]]
   (let [{:keys [id]} db]
     {:db db
      ::fx/ws-send {:q :rematch :id id :turn-no turn-no}})))

(rf/reg-event-fx
 :rematch
 [common-evt/interceptors]
 (fn handle-rematch [{:keys [db]} [_ id]]
   {:db db
    ::fx/location! (str "/game/" id)}))

(rf/reg-event-fx
 ::resign
 [common-evt/interceptors]
 (fn initiate-resign [{:keys [db]} [_ player]]
   (let [{:keys [id check-board]} db]
     {:db (-> db
              (assoc :turn-log [])
              (assoc :board check-board))
       ::fx/ws-send {:q :resign :id id :player player}})))

(rf/reg-event-db
 :resign
 [common-evt/interceptors]
 (fn handle-resign [db [_ data]]
   (let [{:keys [victor mode]} data]
     (if (= (:id data) (:id db))
       (-> db
           (assoc :mode :victory)
           (assoc :turn-log [])
           (assoc :message {:type :info :key (case victor :p1 :p1-wins :p2 :p2-wins nil)}))
       db))))

(rf/reg-event-fx
 ::new-game
 [common-evt/interceptors]
 (fn initiate-new-game [{:keys [db]} _]
   {:db db
    ::fx/xhttp {:method :post :url "/api/game" :handler-key :new-game}}))

(rf/reg-event-fx
 :new-game
 [common-evt/interceptors]
 (fn handle-new-game [{:keys [db]} [_ id]]
   {:db (-> db (dissoc :victor :message :board :player :id :single-player?))
    ::fx/set-local-storage ["storage" (assoc (:storage db)
                                             id {:id id :actor :p1 :single-player? false})]
    ::fx/location! (str "/game/" id "?actor=p1")}))
