(ns push.game.views
  (:require [clojure.spec.alpha :as s]
            [reagent.core :as r]
            [re-frame.core :as rf]
            [push.data :as data]
            [push.common.components :as common-view]
            [push.game.subs :as sub]
            [push.game.events :as evt]))

(s/fdef draw-edges :args (s/cat :side #{:l :r}))
(defn draw-edges [side]
  (let [edges [[:div.edge]
               [:div.edge]
               [:div.edge.block]
               [:div.edge.block]
               [:div.edge.block]
               [:div.edge.block]
               [:div.edge.block]
               [:div.edge]
               [:div.edge]
               [:div.edge]]]
    (case side
      :r (vec (cons :div (reverse edges)))
      :l (vec (cons :div edges)))))

(s/fdef draw-piece :args (s/cat :loc ::data/loc))
(defn draw-piece [loc]
  (let [piece  (rf/subscribe [::sub/piece loc])
        anchor (rf/subscribe [::sub/anchor])
        origin (rf/subscribe [::sub/origin loc])]
    (fn []
      (let [attributes {:class         (when (= loc @origin) "selected")
                        :on-click      #(do (.stopPropagation %)
                                            (rf/dispatch [::evt/select loc @piece]))
                        :draggable     true
                        :on-drag-start #(do (.setData (.-dataTransfer %) "text/plain" "")
                                            (rf/dispatch [::evt/select loc @piece]))
                        :on-drag-end   #(.log js/console "on-drag-end")
                        :on-drag-enter #(.preventDefault %)}]
        (case [(:player @piece) (:type @piece)]
          [:p1 :pawn]   [:div.piece.pawn.p1-color attributes]
          [:p2 :pawn]   [:div.piece.pawn.p2-color attributes]
          [:p1 :pusher] [:div.piece.pusher.p1-color attributes
                         (when (= loc @anchor) [:div.anchor])]
          [:p2 :pusher] [:div.piece.pusher.p2-color attributes
                         (when (= loc @anchor) [:div.anchor])]
          [:div])))))

(s/fdef draw-space :args (s/cat :loc ::data/loc))
(defn draw-space [loc]
  (let [space      (get data/graph loc)
        targetable? (rf/subscribe [::sub/targetable loc])
        attributes {:class        (str (when (not (:off? space)) "on-color")
                                       " "
                                       (when @targetable? "targetable"))
                    :on-click     #(do (.stopPropagation %)
                                       (rf/dispatch [::evt/select loc nil]))
                    :on-drop      #(do (.preventDefault %)
                                       (rf/dispatch [::evt/select loc nil]))
                    :on-drag-over #(do (.preventDefault %))}]
    [:div.space attributes
     [draw-piece loc]]))

(defn draw-board []
  (let [rows (->> (partition 4 (data/generate-board-locs)))]
    [:div.board
     [draw-edges :l]
     [:div
      (for [row rows] ^{:key row}
        [:div.row
         (for [loc row] ^{:key loc}
           [draw-space loc])])]
     [draw-edges :r]]))

(defn action-marker [label consumed?]
  [:div.action-marker {:class (when consumed? "consumed")}
   label])

(s/fdef draw-turn-info :args (s/cat :loc ::data/player))
(defn draw-turn-info [player]
  (let [stage (rf/subscribe [::sub/stage])
        mode (rf/subscribe [::sub/mode])
        active-player (rf/subscribe [::sub/active-player])
        move-count    (rf/subscribe [::sub/action-count #{:move}])
        push-count    (rf/subscribe [::sub/action-count #{:push}])]
    (fn []
      (when (and (= @stage :play)
                 (= @mode :active)
                 (= @active-player player))
        [:div.turn-info {:class player}
         [action-marker "MOVE" (or (>= @move-count 1) (pos? @push-count))]
         [action-marker "MOVE" (or (>= @move-count 2) (pos? @push-count))]
         [action-marker "PUSH" (pos? @push-count)]]))))

(s/fdef draw-setup :args (s/cat :loc ::data/player))
(defn draw-setup [player]
  (let [stage         (rf/subscribe [::sub/stage])
        p1-locs       (data/generate-p1-setup-locs)
        p2-locs       (data/generate-p2-setup-locs)
        active-player (rf/subscribe [::sub/active-player])
        actor         (rf/subscribe [::sub/actor])]
    (fn []
      (when (and (= @stage :setup)
                 (= @active-player player)
                 (= @actor @active-player))
        [:div.setup {:class player}
         (for [loc (case player :p1 p1-locs :p2 p2-locs nil)]
           ^{:key loc} [draw-space loc])]))))

(defn undo-button []
  (let [stage (rf/subscribe [::sub/stage])
        action-count (rf/subscribe [::sub/action-count #{:push :move}])]
    (fn []
      (when (= @stage :play)
        [:button.btn.undo
         {:class ""
          :disabled (zero? @action-count)
          :on-click #(rf/dispatch [::evt/undo])}
         "UNDO"]))))

(defn done-button []
  (let [stage                  (rf/subscribe [::sub/stage])
        push-count             (rf/subscribe [::sub/action-count #{:push}])
        player-setup-complete? (rf/subscribe [::sub/player-setup-complete?])]
    (fn []
      [:button.btn.done
       {:class    ""
        :disabled (or (and (= @stage :play)
                           (< @push-count 1))
                      (and (= @stage :setup)
                           (not @player-setup-complete?)))
        :on-click #(rf/dispatch [::evt/submit])}
       "DONE"])))

(s/fdef draw-buttons :args (s/cat :loc ::data/player))
(defn draw-buttons [player]
  (let [mode          (rf/subscribe [::sub/mode])
        actor         (rf/subscribe [::sub/actor])
        active-player (rf/subscribe [::sub/active-player])]
    (fn []
      (when (and (= @active-player player)
                 (= @mode :active)
                 @actor)
        [:div.buttons {:class player}
         [undo-button]
         [done-button]]))))

(defn draw-history []
  (let [stage      (rf/subscribe [::sub/stage])
        past       (rf/subscribe [::sub/past])
        future     (rf/subscribe [::sub/future])
        turn-count (rf/subscribe [::sub/turn-count])]
    (fn []
      (when (= @stage :play)
        [:div.history
         [:button.btn
          {:on-click #(rf/dispatch [::evt/history-transit :beginning])
           :disabled (empty? @past)}
          "|<"]
         [:button.btn
          {:on-click #(rf/dispatch [::evt/history-transit :back-turn])
           :disabled (empty? @past)}
          "<<"]
         [:button.btn
          {:on-click #(rf/dispatch [::evt/history-transit :back])
           :disabled (empty? @past)}
          "<"]
         [:div.turn-no @turn-count]
         [:button.btn
          {:on-click #(rf/dispatch [::evt/history-transit :forward])
           :disabled (empty? @future)}
          ">"]
         [:button.btn
          {:on-click #(rf/dispatch [::evt/history-transit :forward-turn])
           :disabled (empty? @future)}
          ">>"]
         [:button.btn
          {:on-click #(rf/dispatch [::evt/history-transit :present])
           :disabled (empty? @future)}
          ">|"]]))))

(defn draw-info []
  (let [message (rf/subscribe [::sub/message])]
    (fn []
      (let [{:keys [type body]} @message]
        [:div.info
         [:span {:class type} body]]))))

(defn draw-resign []
  (let [mode       (rf/subscribe [::sub/mode])
        stage      (rf/subscribe [::sub/stage])
        victor     (rf/subscribe [::sub/victor])
        player     (rf/subscribe [::sub/active-player])
        turn-count (rf/subscribe [::sub/turn-count])]
    (fn []
      [:div.resign
       (when (and (= @mode :active)
                  (= @stage :play))
         [:button.btn {:on-click #(rf/dispatch [::evt/resign @player])} "resign"])
       (when (= @mode :hold)
         [:button.btn {:on-click #(rf/dispatch [::evt/rematch @turn-count])}
          (str "fork from turn: " @turn-count)])
       (when (and (= @mode :hold)
                  @victor)
         [:button.btn {:on-click #(rf/dispatch [::evt/new-game])} "new game"])])))

(defn draw-home-link []
  [:a.home-link {:href "/"} "home"])

(defn draw-topbar []
  (let [collapse   (r/atom true)
        mode       (rf/subscribe [::sub/mode])
        stage      (rf/subscribe [::sub/stage])
        player     (rf/subscribe [::sub/active-player])
        turn-count (rf/subscribe [::sub/turn-count])
        single-player? (rf/subscribe [::sub/single-player?])]
    (fn []
      [:div.topbar
       [:a.home-link {:href "/"} "Shove Tussle"]
       [:ul.menu
        [:li.menu-item.menu-head
         [:a {:href     "#"
              :on-click #(swap! collapse not)}
          "menu "
          [:span {:style {"fontFamily" "monospace"}} (if @collapse "+" "-")]]]
        [:ul.menu-options {:class (when @collapse "collapse")}
         [:li.menu-item
          [:a {:href "#"
               :on-click #(rf/dispatch [::evt/toggle-singleplayer])}
           [:span "single player?"]
           [:span (if @single-player? "true" "false")]]]
         [:li.menu-item
          [:a {:href "#"
               :on-click #(rf/dispatch [::evt/new-game])}
           "new game"]]
         [:li.menu-item
          [:a {:href "#"
               :on-click #(rf/dispatch [::evt/rematch @turn-count])
               :disabled ((complement pos?) @turn-count)}
           "fork from turn: " @turn-count]]
         [:li.menu-item
          [:a {:href     "#"
               :on-click #(rf/dispatch [::evt/resign @player])
               :disabled (not (and (= @mode :active)
                                   (= @stage :play)))}
           "resign"]]]]])))

(defn draw-share []
  (let [actor          (rf/subscribe [::sub/actor])
        single-player? (rf/subscribe [::sub/single-player?])]
    (fn []
      (let [opponent (if (= :p1 @actor) "p2" "p1")
            origin   (.-origin (.-location js/window))
            path     (.-pathname (.-location js/window))]
        [:div.share {:style (when @single-player? {:visibility "hidden"})}
         [:p "Share this link with your opponent:"]
         [:input {:type      :text
                  :on-change :read-only
                  :value     (str origin path "?actor=" opponent)}]]))))

(defn game-page []
  [:div.game
   [draw-topbar]
   [draw-info]
   [draw-setup :p1]
   [draw-setup :p2]
   [draw-turn-info :p1]
   [draw-turn-info :p2]
   [draw-buttons :p1]
   [draw-buttons :p2]
   [draw-board]
   [draw-history]
   [draw-share]
   [draw-resign]
   #_[common-view/dev-panel]])
