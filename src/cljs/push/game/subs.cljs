(ns push.game.subs
  (:require [clojure.spec.alpha :as s]
            [clojure.set :as set]
            [re-frame.core :as rf]
            [push.locale :as i18n]
            [push.game :as game]
            [push.data :as data]))

(rf/reg-sub
 ::board
 (fn [db _]
   (:board db)))

(rf/reg-sub
 ::piece
 (fn [db [_ loc]]
   (when (seq (:board db))
     (game/lookup-piece (:board db) loc))))

(rf/reg-sub
 ::anchor
 (fn [db _]
   (get-in db [:board :anchor])))

(rf/reg-sub
 ::mode
 (fn [db _]
   (:mode db)))

(rf/reg-sub
 ::victor
 (fn [db _]
   (:victor db)))

(rf/reg-sub
 ::stage
 (fn [db _]
   (game/stage (:past (:history db)))))

(rf/reg-sub
 ::actor
 (fn [db _]
   (:actor db)))

(rf/reg-sub
 ::active-player
 (fn [db _]
   (:player db)))

(rf/reg-sub
 ::single-player?
 (fn [db _]
   (:single-player? db)))

(rf/reg-sub
 ::player-setup-complete?
 (fn [db _]
   (let [board (:board db)
         active-player (:player db)]
     (game/player-setup-complete? board active-player))))

(rf/reg-sub
 ::message
 (fn [db _]
   (let [message (:message db)]
     (assoc message :body (i18n/localize :en (:key message))))))

(rf/reg-sub
 ::origin
 (fn [db [_ loc]]
   (:origin db)))

(rf/reg-sub
 ::action-count
 (fn [db [_ action-pred]]
   (game/action-count (:turn-log db) action-pred)))

(rf/reg-sub
 ::turn-count
 (fn [db _]
   (game/action-count (:past (:history db)) #{:push})))

(rf/reg-sub
 ::past
 (fn [db _]
   (remove #(= (:type %) :place) (:past (:history db)))))

(rf/reg-sub
 ::future
 (fn [db _]
   (:future (:history db))))

(rf/reg-sub
 ::targetable
 (fn [db [_ loc]]
   (if-let [origin (:origin db)]
     (let [{:keys [board history]} db
           {:keys [type]}       (game/lookup-piece board origin)
           legal-move-targets   (case (game/stage (:past history))
                                  :setup (game/legal-setup-locs board origin)
                                  :play  (game/legal-play-locs board origin))

           legal-push-targets   (as-> (get data/graph origin) $
                                  (:neighbors $)
                                  (vals $)
                                  ;; create actions
                                  (map (fn [target] {:type :push :shifts [[origin target]] :anchor target}) $)
                                  ;; validate actions
                                  (map (partial game/validate-action db) $)
                                  ;; remove invalid pushes
                                  (remove :error $)
                                  ;; return valid push locs
                                  (map (fn [v] (get-in v [:action :anchor])) $))
           targetable-spaces    (if (= type :pusher)
                                  (apply conj legal-move-targets legal-push-targets)
                                  legal-move-targets)]
       (contains? targetable-spaces loc)))))
