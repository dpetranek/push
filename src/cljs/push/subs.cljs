(ns push.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
 ::peek
 (fn [db _] db))

(rf/reg-sub
 ::ws-msgs
 (fn [db _] (:messages db)))

(rf/reg-sub
 ::modal
 (fn [db]
   (:modal db)))
