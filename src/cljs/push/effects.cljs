(ns push.effects
  (:require [accountant.core :as accountant]
            [ajax.core :as ajax]
            [cljs.reader :as reader]
            [re-frame.core :as rf]
            [push.websockets :as ws]))


(rf/reg-fx
 ::xhttp
 (fn xhttp [spec]
  (let [{:keys [method url handler-key params]} spec
        ajax-method (method {:get ajax/GET :post ajax/POST})]
    (ajax-method url
                 {:handler #(rf/dispatch [handler-key (reader/read-string %)])
                  :error-handler #(rf/dispatch [:push.game.events/error
                                                {:type :error :key :network-request-error}])
                  :params params}))))

(rf/reg-fx
 ::ws-send
 (fn ws-send [msg] (ws/send-transit-msg! msg)))

(rf/reg-fx
 ::location!
 (fn location! [url]
   (accountant/navigate! url)))

(rf/reg-fx
 ::set-local-storage
 (fn set-local-storage [[key edn]]
   (.setItem js/localStorage key (str edn))))

(rf/reg-fx
 ::add-keydown-listener
 (fn add-keydown-listener [[named-handler-not-a-lambda]]
   (.addEventListener js/document "keydown" named-handler-not-a-lambda)))

(rf/reg-fx
 ::remove-keydown-listener
 (fn remove-keydown-listener [[named-handler-not-a-lambda]]
   (.removeEventListener js/document "keydown" named-handler-not-a-lambda)))

(rf/reg-fx
 ::add-scroll-listener
 (fn add-scroll-listener [[named-handler-not-a-lambda]]
   (-> (.getElementsByTagName js/document "body")
       (aget 0)
       (.-classList)
       (.add "noscroll"))
   (.addEventListener js/document "scroll" named-handler-not-a-lambda)))

(rf/reg-fx
 ::remove-scroll-listener
 (fn remove-scroll-listener [[named-handler-not-a-lambda]]
   (-> (.getElementsByTagName js/document "body")
       (aget 0)
       (.-classList)
       (.remove "noscroll"))
   (.removeEventListener js/document "scroll" named-handler-not-a-lambda)))
