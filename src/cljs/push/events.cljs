(ns push.events
  (:require [clojure.spec.alpha :as s]
            [accountant.core :as accountant]
            [re-frame.core :as rf]
            [ajax.core :as ajax]
            [push.data :as data]
            [push.coeffects :as cofx]
            [push.effects :as fx]))

(def initial-state
  {:storage {}
   :check-board {}
   :history {:past [] :future []}
   :turn-log []
   :single-player? false
   :mode :hold})

(def interceptors [(when ^boolean goog.DEBUG rf/debug)])

(rf/reg-event-fx
 ::initialize
 [interceptors (rf/inject-cofx ::cofx/get-local-storage "storage")]
 (fn [{:keys [db] :as context} _]
   (let [storage (get context "storage")]
     {:db (-> db
              (merge initial-state)
              (cond-> storage (assoc :storage storage)))})))

(defn prevent-scroll [event]
  (.scroll js/window 0 0))

(defn modal-keypress-handler [event]
  (when (= "Escape" (.-key event))
    (rf/dispatch [::close-modal])))

(rf/reg-event-fx
 ::show-modal
 [interceptors]
 (fn show-modal [{:keys [db]} [_ modal-content]]
   {:db (-> db
            (assoc :modal {:show true :content modal-content}))
    ::fx/add-keydown-listener [ modal-keypress-handler]
    ::fx/add-scroll-listener [prevent-scroll]}))

(rf/reg-event-fx
 ::close-modal
 [interceptors]
 (fn close-modal [{:keys [db]}]
   {:db (-> db
            (assoc :modal {:show false}))
    ::fx/remove-keydown-listener [modal-keypress-handler]
    ::fx/remove-scroll-listener [ prevent-scroll]}))
