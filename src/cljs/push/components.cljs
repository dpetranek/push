(ns push.components
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [push.subs :as sub]
            [push.events :as common-evt]))

(defn dev-panel []
  (let [db (rf/subscribe [::sub/peek])]
    (fn []
      [:div.dev
       [:div (str @db)]])))

(defn modal
  "needs to prevent scrolling,
  close on esc, off-click, x-button, or cancel"
  [content]
  [:div.modal-bg {:on-click #(do (.log js/console "close plz")
                                 (rf/dispatch [:hide-modal]))}
   [:div.modal-fg
    [:span.close "x"]
    content]])

(rf/reg-event-db
 ::show-modal
 [common-evt/interceptors]
 (fn show-modal [db [_]]
   (-> db
       (assoc :modal true))))

(rf/reg-event-db
 ::close-modal
 [common-evt/interceptors]
 (fn close-modal [db [_]]
   (-> db
       (assoc :modal false))))
