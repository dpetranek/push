(ns push.tips.views
  (:require [re-frame.core :as rf]
            [push.events :as common-evt]))

(def modal-content
  [:p "hello, let's fill up a whole paragraph's amount of space on the screen, then we can hopefully see how this thing plays out. "])

(defn tips-page []
  [:div
   [:h1 "Tips"]
   [:button {:on-click #(rf/dispatch [::common-evt/show-modal modal-content])} "get tips"]
   [:ul
    (for [i (range 50)] ^{:key i}
      [:li i])]])
