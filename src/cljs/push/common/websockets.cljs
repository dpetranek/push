(ns push.common.websockets
  (:require [cognitect.transit :as t]
            [re-frame.core :as rf]
            [push.events :as common-evt]))

(defonce ws-chan (atom nil))
(def json-reader (t/reader :json))
(def json-writer (t/writer :json))

(defn receive-transit-msg!
  [update-fn]
  (fn [msg]
    (update-fn (->> msg .-data (t/read json-reader)))))

(defn send-transit-msg!
  [msg]
  (if @ws-chan
    (.send @ws-chan (t/write json-writer msg))
    (throw (js/Error. "Websocket is not available!"))))

(defn receive-handler [msg]
  (rf/dispatch [::receive-msg msg]))

(defn make-websocket! [url]
  (println "attempting to connect websocket")
  (if-let [chan (js/WebSocket. url)]
    (do
      (set! (.-onmessage chan) (receive-transit-msg! receive-handler))
      (reset! ws-chan chan)
      (println "Websocket connection established with: " url))
    (do (.error js/console "Websocket connection failed!")
        (.setTimeout js/window (fn []
                                 (.log js/console "Retrying websocket connection")
                                 (make-websocket! url))
                     2000))))

(rf/reg-event-fx
 ::receive-msg
 [common-evt/interceptors]
 (fn [{:keys [db]} [_ msg]]
   (let [{:keys [r data]} msg]
     {:dispatch [r data]
      :db db})))

(rf/reg-event-fx
 ::send-msg
 [common-evt/interceptors]
 (fn [{:keys [db]} [_ msg]]
   {:db db
    ::ws-send msg}))
