(ns push.common.components
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [push.subs :as sub]
            [push.events :as common-evt]))

(defn dev-panel []
  (let [db (rf/subscribe [::sub/peek])]
    (fn []
      [:div.dev
       [:div (str @db)]])))

(defn modal
  "needs to prevent scrolling"
  [content]
  [:div.modal-bg {:on-click #(rf/dispatch [::common-evt/close-modal])
                  :on-key-up #(.log js/console "keyup" %)}
   [:div.modal-fg {:on-click #(.stopPropagation %)}
    [:div.modal-header [:span.close {:on-click #(rf/dispatch [::common-evt/close-modal])} "+"]]
    content]])
