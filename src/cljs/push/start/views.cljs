(ns push.start.views
  (:require [re-frame.core :as rf]))

(defn start-page []
  (let []
    (fn []
      [:div.start-page {:style {:display "flex" :flex-direction "column"}}
       [:h1 "SHOVE TUSSLE"]
       [:small.subtitle "A mental diversion for the pugnacious and attentive"]
       [:br]
       [:button.btn.start-game
        {:class (if (> (rand-int 101) 50) "p1-color" "p2-color")
         :on-click #(rf/dispatch [:push.game.events/new-game])}
        "New Game"]
       [:p "Shove Tussle is an implementation of " [:a {:href "http://www.pushfightgame.com/"} "Push Fight"]
        " by the peerless Brett Picotte. It is an abstract board game for 1-2 players that takes 5-15 minutes to play."]
       [:h4 "Instructions:"]
       [:code
        [:ol
         [:li "Click 'New Game' to start a new game"]
         [:li "Copy the url below the board and share it with your opponent"]
         [:li "Make sure to copy the url from the location bar for your own reference"]]]
       [:h4 "Rules:"]
       [:code
        "Setup:"
        [:ol
         [:li "Player one places their pieces on the top half of the board"]
         [:li "Player two places their pieces on the bottom half of the board"]]
        "Play:"
        [:ol
         [:li "A player can move 0, 1, or 2 pieces to any other open space without transiting an occupied space"]
         [:li "A player must use a square piece to push at least one other piece one space"]
         [:li "After pushing, the pusher piece is anchored and cannot be pushed"]]
        "Victory:" [:br] [:br]
        "The first player to push an opponent's piece off an edge wins!" ]
       [:p.disclaimer "Shove Tussle is a work in progress. If the server crashes (or I restart it in a fit of capriciousness) you'll lose your saved games, sorry."]])))
