(ns push.coeffects
  (:require [cljs.reader :as reader]
            [re-frame.core :as rf]))

(rf/reg-cofx
 ::get-local-storage
 (fn get-local-storage [context key]
   (assoc context key (reader/read-string (.getItem js/localStorage key)))))
