(ns push.db
  (:require [push.data :as data]))

(def initial-state
  {:next-id 2
   :games
   {1 {:id 1 :board data/setup}}
   :history []})

(defonce db (atom initial-state))

(defn next-id []
  (let [next-id (:next-id @db)]
    (swap! db update :next-id inc)
    next-id))

(comment
  @db

  (swap! db (fn [db]
              (assoc db :users
                     (mapv (fn [i] {:id i}) (range 17)))))

  (defn select-users [& [opts]]
    (let [users (:users @db)
          {:keys [start end limit]} opts]
      (if opts
        (let [window (take limit (drop start users))
              total (count users)
              next-end (+ start limit)]
          {:data (vec window)
           :total total
           :start start
           :end (if (> next-end total)
                  total
                  next-end)
           :limit limit})
        {:data users
         :total (count users)})))

  (select-users)
  (select-users {:limit 10 :start 0})
  (select-users {:limit 10 :start 10})
  {:data [{:id 10} {:id 11} {:id 12} {:id 13} {:id 14} {:id 15} {:id 16}],
   :total 17,
   :start 10,
   :end 17,
   :limit 10}


  (reset! db initial-state)
  )
