(ns push.routes.websockets
  (:require [cognitect.transit :as t]
            [compojure.core :refer [GET defroutes]]
            [clojure.tools.logging :as log]
            [immutant.web.async :as async]
            [push.session :as session])
  (:import [java.io ByteArrayInputStream ByteArrayOutputStream]))


(def msg-inspector (atom nil))

(defn tjson->edn [data]
  (t/read (t/reader (ByteArrayInputStream. (.getBytes data)) :json)))

(defn edn->tjson [data]
  (let [out (ByteArrayOutputStream. 2000)]
    (t/write (t/writer out :json) data)
    (str out)))

(defn broadcast! [msg]
  (log/info "ws-broadcast" msg)
  (doseq [channel (:channels @session/session)]
    (async/send! channel (edn->tjson msg))))

(defn send! [channel msg]
  (log/info "ws-send" msg)
  (async/send! channel (edn->tjson msg)))

(defmulti handle-msg
  (fn [channel msg] (:q msg)))

(defmethod handle-msg :default [channel msg]
  (log/info "default handler" msg)
  (reset! msg-inspector msg)
  (doseq [channel (:channels @session/session)]
    (send! channel (assoc msg :r (:q msg) :data msg))))

(defn receive-websocket [channel msg]
  (let [edn-msg (tjson->edn msg)]
    (log/info "ws-recieve" edn-msg)
    (handle-msg channel edn-msg)))

(defn connect! [channel]
  (log/info "channel open")
  (swap! session/session #(update % :channels conj channel)))

(defn disconnect! [channel {:keys [code reason]}]
  (log/info "close code:" code "reason:" reason)
  (swap! session/session
         (fn [s] (update s :channels #(->> % (remove #{channel}) (into #{}))))))

(def websocket-callbacks
  "WebSocket callback functions"
  {:on-open connect!
   :on-close disconnect!
   :on-message receive-websocket})

(defn ws-handler [request]
  (async/as-channel request websocket-callbacks))

(defroutes websocket-routes
  (GET "/ws" [] ws-handler))
