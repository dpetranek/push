(ns push.routes.game
  (:require [clojure.tools.logging :as log]
            [clojure.spec.alpha :as s]
            [push.routes.websockets :as ws]
            [push.db :as db]
            [push.game :as game]
            [push.data :as data]))


(s/def ::history ::data/action-stream)
(s/fdef fetch-game
  :args (s/or :effectful (s/cat :id ::data/id)
              :pure (s/cat :db any? :id ::data/id))
  :ret (s/keys :req-un [::data/check-board ::history ::data/id]))
(defn fetch-game
  "Fetch the board and history for a given game id."
  ([id]
   (fetch-game @db/db id))
  ([db id]
   (let [{:keys [id board victor]} (get-in db [:games id])
         history (->> (:history db) (filter #(= (:id %) id)) (vec))]
     (cond-> {:id id
              :check-board board
              :history history}
       victor (assoc :victor victor)))))

(s/fdef update-history
  :args (s/cat :id ::data/id :history ::history)
  :ret ::history)
(defn update-history [id turn history]
  (apply conj history (map #(assoc % :id id) turn)))

(s/fdef update-game
  :args (s/cat :board ::data/board :turn ::data/turn-log :history ::history)
  :ret (s/keys :req-un [::data/board ::history]))
(defn update-game [board turn id]
  (let [victor (case (game/get-loser board) :p1 :p2 :p2 :p1 nil)
        next-db (swap! db/db (fn [db]
                               (-> db
                                   (update-in [:games id] assoc :board board)
                                   (cond-> victor (update-in [:games id] assoc :victor victor))
                                   (update-in [:history] (partial update-history id turn)))))]
    (fetch-game next-db id)))

(s/fdef check-and-update-game
  :args (s/cat :state (s/keys :req-un [::data/id ::data/check-board ::data/turn-log ::data/player]))
  :ret (s/keys :req-un [::data/board ::history]))
(defn check-and-update-game [state]
  (let [{:keys [id check-board turn-log player]} state
        stored-board          (:check-board (fetch-game id))
        server-state          {:board        stored-board
                               :check-board  stored-board
                               :player       player
                               :turn-log []}
        {:keys [board error]} (game/validate-and-apply-turn server-state turn-log)]
    (cond (not= stored-board check-board)
          (assoc state :error {:type :error :key :invalid-commit3})
          error
          (do (log/error error)
              (assoc state :error {:type :error :key :invalid-commit4}))
          :else
          (update-game board turn-log id))))

(s/fdef rematch-game
  :args (s/cat :id ::data/id :turn-no nat-int?)
  :ret ::data/id)
(defn rematch-game [id turn-no]
  (let [next-id (db/next-id)
        history (->> (:history @db/db)
                     ;; stupid expensive
                     (filter #(= (:id %) id))
                     ;; stupid expensive
                     (reduce (fn [history action]
                               (if (= (count (filter #(= (:type %) :push) history)) turn-no)
                                 (reduced history)
                                 (conj history action)))
                             [])
                     (mapv (fn [action]
                             (assoc action :id next-id))))
        board (game/apply-actions data/setup history)]
    (swap! db/db (fn [db]
                   (println "rematching" history board)
                   (-> db
                       (assoc-in [:games next-id] {:id next-id :board board})
                       (update :history #(apply conj % history)))))
    next-id))

(s/fdef new-game
  :ret ::data/id)
(defn new-game []
  (let [next-id (db/next-id)]
    (swap! db/db (fn [db] (assoc-in db [:games next-id] {:id next-id :board data/setup})))
    next-id))

(s/fdef resign
  :args (s/cat :id ::data/id :player ::data/player)
  :ret (s/keys :req-un [::data/id ::data/mode :data/victor]))
(defn resign [id player]
  (let [victor (if (= player :p1) :p2 :p1)]
    (swap! db/db (fn [db]
                   (-> db
                       (update-in [:games id] assoc :victor victor))))
    {:id id :mode :victory :victor victor}))


(defmethod ws/handle-msg :fetch-game [channel msg]
  (let [{:keys [id]} msg]
    (ws/send! channel {:r :fetch-game :data (fetch-game id)})))

(defmethod ws/handle-msg :update-game [channel msg]
  (ws/broadcast! {:r :update-game :data (check-and-update-game msg)}))

(defmethod ws/handle-msg :resign [channel msg]
  (let [{:keys [id player]} msg]
    (ws/broadcast! {:r :resign :data (resign id player)})))

(defmethod ws/handle-msg :rematch [channel msg]
  (let [{:keys [id turn-no]} msg]
    (ws/send! channel {:r :rematch :data (rematch-game id turn-no)})))
