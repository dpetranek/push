(ns push.routes.api
  (:require [compojure.core :refer [GET POST defroutes]]
            [ring.util.http-response :refer [ok not-found]]
            [push.routes.game :as game]))

(defroutes api-routes
  (POST "/api/game" [] (ok (str (game/new-game))))
  (GET "/api/game/:id" [id] (ok (str (game/fetch-game (Integer/parseInt id))))))
