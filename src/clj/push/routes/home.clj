(ns push.routes.home
  (:require [push.layout :as layout]
            [compojure.core :refer [defroutes GET]]
            [ring.util.http-response :refer [ok]]))



(defn home-page []
  (layout/base))

(defroutes home-routes
  (GET "/" [] (home-page))
  (GET "/game/:id" [id] (home-page))
  (GET "/tips" [] (home-page)))
