(ns push.layout
  (:require [hiccup.page :as hic]
            [ring.util.http-response :as resp :refer [content-type ok]]
            [ring.util.anti-forgery :as csrf :refer [anti-forgery-field]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [push.config :as config]))


(declare ^:dynamic *app-context*)

(def mount-target
  [:div#app
   [:noscript
    (str "This is a web application that relies on javascript, "
         "but I promise not to do anything antisocial with it, "
         "so if you can enable it, I encourage you to do so!")]
   [:p "Loading..."]])

(defn base []
  (content-type
   (ok
    (hic/html5
     [:head
      [:title "Shove Tussle"]
      #_[:link {:rel "icon" :href "/p2.ico"}]
      [:meta {:charset "utf-8" :content "text/html;" :http-equiv "Content-Type"}]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
      (hic/include-css "/css/site.css")]
     [:body
      mount-target
      [:script {:type "text/javascript"}
       (str "var context = " (str "\"" *app-context* "\"") "; ")
       (str "var csrfToken = " (str "\"" *anti-forgery-token* "\"") "; ")]
      (hic/include-js "/js/app.js")]))
   "text/html; charset=utf-8"))
