(ns push.handler
  (:require [mount.core :as mount]
            [compojure.core :as router]
            [compojure.route :as route]
            [push.env :as env]
            [push.middleware :as middleware]
            [push.routes.websockets :refer [websocket-routes]]
            [push.routes.home :refer [home-routes]]
            [push.routes.api :refer [api-routes]]
            [push.routes.game]
            ))

(mount/defstate init-app
                :start ((or (:init env/defaults) identity))
                :stop  ((or (:stop env/defaults) identity)))

(def handler
  (router/routes
   websocket-routes
   api-routes
   (-> #'home-routes
       (router/wrap-routes middleware/wrap-csrf)
       (router/wrap-routes middleware/wrap-formats))
   (route/not-found
    (:body
     {:status 404
      :title "page not found"
      :body "This page isn't available to users in your location."}))))


(defn app [] (middleware/wrap-base #'handler))
