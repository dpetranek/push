(ns push.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[push started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[push has shut down successfully]=-"))
   :middleware identity})
