(ns push.env
  (:require [clojure.tools.logging :as log]
            [push.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[push started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[push has shut down successfully]=-"))
   :middleware wrap-dev})
