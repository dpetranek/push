(ns user
  (:require [mount.core :as mount]
            [push.figwheel :refer [start-fw stop-fw cljs]]
            push.core))

(defn start []
  (mount/start-without #'push.core/repl-server))

(defn stop []
  (mount/stop-except #'push.core/repl-server))

(defn restart []
  (stop)
  (start))
