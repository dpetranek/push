(ns ^:figwheel-no-load push.app
  (:require [push.client :as client]
            [devtools.core :as devtools]
            [figwheel.client :as figwheel :include-macros true]))

(enable-console-print!)

(figwheel/watch-and-reload
  :websocket-url "ws://localhost:3449/figwheel-ws"
  :on-jsload client/mount-root)

(devtools/install!)

(client/init!)
