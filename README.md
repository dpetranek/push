# about

This is an implementation of Brett Picotte's wonderful [Push Fight](http://www.pushfightgame.com/).

# development

Fire up a repl and run `(start)` to start the web server and then `(start-fw)` to start figwheel.

Or, in one terminal run `lein run` and in another run `lein figwheel`.

# build

Run `lein uberjar` to build a jar fit for distribution.

# License
AGPL-3.0

Copyright � 2017 Dan Petranek
