(ns push.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [push.core-test]))

(doo-tests 'push.core-test)
